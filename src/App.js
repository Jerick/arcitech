import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CreateList from "./Screens/CreateList";

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={CreateList} />
      <Route exact path="/:list" component={CreateList} />
    </Switch>
  </Router>
);

export default App;
