import React from 'react';
import { withRouter } from "react-router";
import { Container, Row, Button, Jumbotron, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { generateSideBar, createUrl, parseUrl, getFaction, getSize, updateTextList } from '../Data/Utils';
import ListRow from '../Components/ListRow';
import MenuRow from '../Components/MenuRow';
import LoadoutRow from '../Components/LoadoutRow';
import CadreRow from '../Components/CadreRow';
import { Faction, Size, Keywords } from "../Data/Enums";

const initialState = {
    size: '',
    heroLimit: 0,
    unitLimit: 0,
    faction: '',
    factionName: '',
    list: [],
    error: [],
    textList: '',
}

class CreateList extends React.Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    componentDidMount = () => {
        const { match } = this.props;        
        if (match.params['list']) {
            this.setState({ listString: match.params['list'], ...parseUrl(match.params['list']) })
        }        
    };

    componentDidUpdate = () => {
        window.onpopstate = e => {
            if(this.props.match.params.list) {
                this.setState({ ...initialState, listString: this.props.match.params['list'], ...parseUrl(this.props.match.params['list']) })
            } else {
                this.setState({ ...initialState});
            }
         }
      }

    getUnitCount = (unit) => {
        const { list } = this.state;
        return list.filter(u => (u.name === unit.name)).length;
    };

    getFreeHeroSlots = (list) => {
        return this.state.heroLimit - list.filter(unit => (unit.isHero === true && !unit.isVehicle)).length;
    }

    getFreeUnitSlots = (list) => {
        let totalCount = 0;
        list.forEach(unit => {
            if(!unit.isHero) {
                if(unit.slotCost) {
                    totalCount += unit.slotCost;
                } else {
                    totalCount++;
                }
            }
        })

        return this.state.unitLimit - totalCount;
    }

    canAddUnit = (unit) => {
        const { list } = this.state;
        let freeUnitSlots = this.getFreeUnitSlots(list);
        let freeHeroSlots = this.getFreeHeroSlots(list);

        let freeTotalSlots = freeUnitSlots + freeHeroSlots;
        if(unit.isHero && freeHeroSlots > 0){
            return true;
        } else if(unit.isHero && freeTotalSlots > 0) {
            return true;
        } else {
            if(unit.slotCost && freeUnitSlots > unit.slotCost && freeTotalSlots > unit.slotCost) {
                return true;
            } else {
                if(!unit.slotCost && freeUnitSlots > 0 && freeTotalSlots > 0) {
                    return true;
                }
                return false;
            } 
        }
    }

    addUnit = (unit) => {                
        if (this.canAddUnit(unit)) {
            if (this.getUnitCount(unit) < (unit.limit || 4)) {
                this.setState({ list: [...this.state.list, {...unit}] }, this.updateHistory)
            }
        } else {
            // List is full!
        }
    };

    resetList = () => {
        this.setState({ list: [] }, this.updateHistory);
    }

    removeUnit = (unit) => {
        const { list } = this.state;
        let index = -1;
        list.forEach((u, i) => {
            if (unit.isWarjack && u.isWarjack) {
                if ((JSON.stringify(u.weaponList) === JSON.stringify(unit.weaponList)) && (JSON.stringify(u.cortex) === JSON.stringify(unit.cortex))) index = i;
            } else if (unit.isVehicle && u.isVehicle) {
                if (JSON.stringify(u.weaponList) === JSON.stringify(unit.weaponList)) index = i;
            } else {
                if (u.name === unit.name) index = i;
            }
        })
        let newArray = list;
        newArray.splice(index, 1);
        
        this.setState({ list: [...newArray] }, this.updateHistory);
    }

    updateLoadout = (index, unit) => {
        let newList = this.state.list;
        newList[index] = unit;

        this.setState({list: newList}, this.updateHistory);
    }

    updateHistory = () => {
        const { history } = this.props;    
        let url = createUrl(this.state);
        this.setState({textList: updateTextList(this.state.list, url, this.state.factionName)});
        history.push(url);
    }

    renderList = () => {
        const { list } = this.state;       
        let flatList = [];
        list.forEach((unit, index) => {
            let i = flatList.findIndex(u => { return (!unit.hardpoints && u.name === unit.name) });
            if(i < 0) {                
                flatList.push({...unit, unitCount: this.getUnitCount(unit), index: index})
            }
        })

        return (
            <div className='flex flex-column ml1 mt1'>
                {flatList.map((unit, index) => {
                    if (unit.hardpoints) {
                        return (<LoadoutRow unit={unit} index={unit.index} key={unit.name + unit.index} updateLoadout={this.updateLoadout} onClick={() => this.removeUnit(unit)} />);
                    } else if(unit.keywords?.includes(Keywords.Cadre)) {
                        return (<CadreRow cadre={unit} faction={this.state.faction} key={unit.name + index} onClick={() => this.removeUnit(unit)} />);
                    } else {
                        return (<ListRow unit={unit} key={unit.name + index} onClick={() => this.removeUnit(unit)} />);
                    }
                })}
            </div>
        );
    }

    renderMenu = () => {
        let sidebar = generateSideBar(this.state.faction);
        let menu = [];        
        Object.keys(sidebar).forEach((key) => {
            menu.push(
                <div className='flex flex-column' key={key}>
                    <div className='f5 ttc bb b mb1 orbitron'>{key}</div>
                    <div className='flex flex-column f5 roboto'>
                        {sidebar[key].map((unit, index) => {
                            let newUnit = JSON.parse(JSON.stringify(unit));                            
                            return <MenuRow className='ml1 pb1' name={unit.name} key={key + unit.name + index} onClick={() => this.addUnit(newUnit)} />
                        })}
                    </div>
                </div>
            )
        });
        return menu;
    }

    renderSelectSize = () => {        
        return (
            <Container fluid='sm'>
                <Row>
                    <Jumbotron>
                        <h1>Arcitech.net</h1>
                        <p>
                            This website has been created to facilitate in the creation of amy lists for the game Warcaster.
                        </p>
                        <p>
                            Please select a size of list below.
                        </p>
                        <Row className='justify-center'>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={() => { this.setState({ ...getSize(Size.Intro) }, this.updateHistory); }}>
                                Intro (6/0)
                                    </Button>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={
                                    () => { this.setState({ ...getSize(Size.Skirmish) }, this.updateHistory); }}>
                                Skirmish (8/1)
                                    </Button>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={
                                    () => { this.setState({ ...getSize(Size.MidSized) }, this.updateHistory); }}>
                                Mid Sized (11/2)
                                    </Button>

                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={
                                    () => { this.setState({ ...getSize(Size.Full) }, this.updateHistory); }} >
                                Full Sized (15/3)
                                    </Button>

                        </Row>
                    </Jumbotron >
                </Row>
            </Container >
        );
    }

    renderSelectFaction = () => {        
        return (
            <Container fluid='sm'>
                <Row>
                    <Jumbotron>
                        <h1>Arcitech.net</h1>
                        <p>
                            Select a faction:
                </p>
                        <Row className='justify-center'>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={() => { this.setState({ ...getFaction(Faction.MW) }, this.updateHistory); }}> Marcher Worlds
                            </Button>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={() => { this.setState({ ...getFaction(Faction.ISA) }, this.updateHistory); }}> Iron Star Alliance
                            </Button>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={() => { this.setState({ ...getFaction(Faction.AC) }, this.updateHistory); }}> Aeternus Continuum
                            </Button>
                            <Button
                                block
                                variant="primary"
                                className='mh2'
                                onClick={() => { this.setState({ ...getFaction(Faction.EMP) }, this.updateHistory); }}> Empyrean
                            </Button>
                        </Row>
                    </Jumbotron >
                </Row>
            </Container >
        );
    }

    render() {
        const { faction, size, list, factionName, textList, logo } = this.state;
        
        let heroSlots = this.getFreeHeroSlots(list);
        let unitSlots = this.getFreeUnitSlots(list);
        if(heroSlots < 0) unitSlots += heroSlots;
        return (
            <div className='flex flex-column ph1 w-100'>
                {!size && !faction && this.renderSelectSize()}
                {size && !faction && this.renderSelectFaction()}
                {faction && size &&
                    <>
                        <div className='flex flex-row w-100 bb justify-between'>
                            <div className='flex f3 b orbitron items-center'><Image className='mr2' src={'./img/' + logo} width={40} height={40}/>{factionName}</div>
                            <div className='flex flex-row'>
                                <div className='ml3 f3 green items-center flex pointer dim' onClick={() => {navigator.clipboard.writeText(textList)}}><FontAwesomeIcon size='1x' icon="clipboard" /></div>
                                <div className='ml3 f3 green items-center flex pointer dim' onClick={() => { }}><FontAwesomeIcon size='1x' icon="chart-bar" /></div>
                                <div className='ml3 f3 red items-center flex pointer dim' onClick={this.resetList}><FontAwesomeIcon size='1x' icon="trash" /></div>                
                            </div>
                        </div>
                        <div className='flex flex-row justify-between w-100 bb roboto'>
                            <div className='f5 b'>List Slots Left: {unitSlots}</div>
                            <div className='f5 b'>Hero Slots Left: {heroSlots >= 0 ? heroSlots : 0}</div>
                        </div>
                        <div className='flex flex-row justify-between w-100'>
                            <div className='flex flex-column w-40'>
                                {this.renderMenu()}
                            </div>
                            <div className='flex flex-column w-60'>
                                {this.renderList()}                                
                            </div>
                        </div>
                    </>}
            </div>
        );
    }
}

export default withRouter(CreateList);