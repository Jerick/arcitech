import React from 'react';
import RadioWeapon from './RadioWeapon';
import Accordion from './Accordian';

export default class HardpointAccordian extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedWeapon: ''
        }
    }

    _onClick = (weapon) => {           
        const {setHardpoint, index} = this.props;     
        this.setState({selectedWeapon: weapon});
        setHardpoint(weapon, index);
    }

    render() {
        const { pointsRemaining, hardpoint, weapons, isOpen, label, setHardpoint, index, isCortex } = this.props;  
        const { selectedWeapon } = this.state;
        let cheapest = -1;
        weapons.forEach(w => 
            {
                if(cheapest === -1) cheapest = w.cost;
                if(w.cost < cheapest) cheapest = w.cost;
            });
        let cantAfford = (!selectedWeapon && (cheapest > pointsRemaining))
        let open = isOpen;
        if(cantAfford) open = false;

        let itemLabel = label ? label : (isCortex ? 'Select a Cortex' : (cantAfford ? 'No Weapon' : 'Select a Weapon'))       

        return (
            <div className='flex flex-column w-100'>
                <div className='mb1 bb b orbitron'>
                    {hardpoint}
                </div>
                <Accordion
                    className='w-100 ml2'
                    onToggle={() => setHardpoint(null, index)}
                    childClassName='flex-column ml2 flex-column-spacing-1'
                    label={itemLabel}
                    isOpen={open}>
                    {weapons.map((weapon, index) =>
                        (<RadioWeapon
                            pointsRemaining={pointsRemaining}
                            weapon={weapon}                            
                            key={index}
                            onClick={this._onClick} />))}
                </Accordion>
            </div>
        );
    }
}