import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class ListRow extends React.Component {
    render() {
        const { unit, className, onClick,  } = this.props;
        const { name, isHero, unitCount } = unit
        const backgroundColor = isHero ? 'bg-light-green ' : 'bg-light-green '        
        return (
            <div className={'flex flex-row w-100 justify-between mb1 ph1 ' + backgroundColor + className}>
                <div className={'flex flex-column'} key={name}>
                    <div className='flex flex-row'>
                        <div className='flex items-center b orbitron'>
                            {unitCount ? unitCount + 'x ' : ''} {name}
                        </div>
                        {isHero &&
                            <div className='flex f6 items-center pl1 roboto'>
                                (Hero)
                            </div>}
                    </div>
                </div>
                {onClick &&
                    <div className='flex items-center mh2'>
                        <FontAwesomeIcon className='items-center pointer grow ' size='1x' icon="times-circle" onClick={onClick} />
                    </div>}
            </div>
        );
    }
}