import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

export default class RadioWeapon extends React.Component {
    _onClick = () => {
        const { onClick, weapon } = this.props
        onClick(weapon);
    }

    render() {
        const { pointsRemaining, weapon } = this.props;

        let isEnabled = (pointsRemaining >= (weapon.cost || 0))
        
        let weaponCost = weapon.cost ? '(' + weapon.cost + ')' : '';
        let color = isEnabled ? 'pointer dim ' : 'gray'

        return (
            <div className={'flex flex-row items-center ' + color} onClick={isEnabled ? this._onClick : null}>                
                <FontAwesomeIcon icon="chevron-circle-right" />
                <div className='flex flex-row ml2 roboto'>
                    {weapon.name} {weaponCost}
                </div>
            </div>

        );
    }
} 