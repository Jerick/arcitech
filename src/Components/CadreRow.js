import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { findModel } from '../Data/Utils';
import ListRow from './ListRow';

export default class CadreRow extends React.Component {
    render() {
        const { cadre, className, onClick, faction } = this.props;
        const { name, key, contains, unitCount } = cadre
        const backgroundColor = 'bg-light-green '                
        return (
            <div className={'flex flex-row w-100 justify-between mb1 ph1 ' + backgroundColor + className}>
                <div className={'flex flex-column'} key={key}>
                    <div className='flex flex-column'>
                        <div className='flex items-center b orbitron mb1'>                    
                            {unitCount ? unitCount + 'x ' : ''} {name}
                        </div>
                        <div className='flex flex-column items-center b orbitron ml4'>                            
                            {contains.map((unit, index) => {
                                let foundUnit = findModel(unit, faction);                                
                                return (<ListRow unit={foundUnit} key={unit + index}/>); 
                                })}
                        </div>
                    </div>
                </div>
                {onClick &&
                    <div className='flex items-center mh2'>
                        <FontAwesomeIcon className='items-center pointer grow ' size='1x' icon="times-circle" onClick={onClick} />
                    </div>}
            </div>
        );
    }
}