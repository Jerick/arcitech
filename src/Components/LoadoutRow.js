import React from 'react';
import HardpointAccordian from './HardpointAccordian';
import { getWeapons, getWeaponPoints } from '../Data/Utils';
import { Hardpoint } from '../Data/Enums';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class LoadoutRow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            initialWeaponPoints: getWeaponPoints(this.props.unit),
            weaponPoints: this.props.unit.weaponPoints,
            weaponList: this.props.unit.weaponList || [],
        }
    }

    generateHardpointArray = (unit) => {
        if (!unit.hardpoints) return [];
        let hardpointArray = [];
        unit.hardpoints.forEach((hardpoint) => {
            for (let i = 0; i < hardpoint.count; i++) {
                hardpointArray.push(hardpoint.id);
            }
        });
        return hardpointArray;
    }

    addWeapon = (weapon, index) => {
        const { weaponList, weaponPoints } = this.state;
        let newWeaponList = weaponList;                
        let updatedWeaponPoints = 0;
        if (weapon == null) {
            if (!newWeaponList[index]) return;
            let oldWeapon = newWeaponList[index];
            delete newWeaponList[index];
            updatedWeaponPoints = weaponPoints + (oldWeapon.isCortex ? 0 : oldWeapon.cost);
            this.setState({ weaponList: newWeaponList, weaponPoints: updatedWeaponPoints }, this.props.updateLoadout(this.props.index, {...this.props.unit, weaponList, weaponPoints: updatedWeaponPoints}));
        } else {
            if (weapon.cost > weaponPoints) {
                // Can't Add Weapon No Points Available
            } else {
                newWeaponList[index] = weapon;
                updatedWeaponPoints = weaponPoints - (weapon.isCortex ? 0 : weapon.cost);
                this.setState({ weaponList: newWeaponList, weaponPoints: updatedWeaponPoints }, this.props.updateLoadout(this.props.index, {...this.props.unit, weaponList, weaponPoints: updatedWeaponPoints}));
            }
        }

        return;
    }

    clearWeapons = () => {
        this.setState({ weaponList: [], weaponPoints: this.state.initialWeaponPoints}, this.props.updateLoadout(this.props.index, {...this.props.unit, weaponList: [] , weaponPoints:  this.state.initialWeaponPoints}));
    }

    removeWeapon = (weapon) => {
        const { weaponList, weaponPoints } = this.state;
        let newArray = weaponList;
        newArray.splice(weaponList.findIndex(w => { return w.name === weapon.name }), 1);
        this.setState({ weaponList: newArray, weaponPoints: weaponPoints + weapon.cost }, this.props.updateLoadout(this.props.index, {...this.props.unit, weaponList, weaponPoints:  weaponPoints + weapon.cost}));
    }

    render() {
        const { unit, className, onClick } = this.props;
        const { weaponPoints, weaponList } = unit;
        const { name, isHero } = unit;
        const backgroundColor = isHero ? 'bg-light-green ' : 'bg-light-green '
        const hardpointArray = this.generateHardpointArray(unit);
        const weapons = getWeapons(unit);

        let cheapest = -1;
        weapons.forEach(w => 
            {
                if(cheapest === -1) cheapest = w.cost;
                if(w.cost < cheapest) cheapest = w.cost;
            });
        let selectedCount = 0;
        let cortextSelected = false;
        weaponList.forEach(w => {
            if(w.isCortex) {
                cortextSelected = true;
            }
            selectedCount++;
        }) 
        let cantAfford = ((cheapest > weaponPoints && cortextSelected) || (selectedCount === hardpointArray.length))

        return (
            <div className={'flex flex-row w-100 justify-between mb1 ph1 ' + backgroundColor + className}>
                <div className={'flex flex-column w-100'} key={name}>
                    <div className='flex flex-column'>
                        <div className='flex flex-row justify-between'>
                            <div className='flex flex-row'>
                                <div className='flex items-center b orbitron'>
                                    {name}                            
                                </div>
                                {isHero &&
                                    <div className='flex f6 items-center pl1 roboto'>
                                        (Hero)
                                </div>}
                            </div>
                            {onClick &&
                                <div className='flex items-center mh2'>
                                    <FontAwesomeIcon className='items-center pointer grow ' size='1x' icon="times-circle" onClick={onClick} />
                                </div>}
                        </div>

                        {(isHero || !cantAfford) && <div className='flex flex-row justify-between roboto'>                        
                            {!cantAfford && <div className='flex f6 items-center pl1 '>
                                Weapon Points Left: {weaponPoints}
                            </div>}
                        </div>}
                    </div>

                    {!!hardpointArray && !!weaponList && !cantAfford && hardpointArray.map((hardpoint, index) => {
                        if (hardpoint === Hardpoint.Cortex) {
                            return <HardpointAccordian
                                pointsRemaining={weaponPoints}
                                hardpoint={hardpoint}
                                index={index}
                                key={index}
                                label={weaponList[index]?.name}
                                isOpen={!weaponList[index]}
                                isCortex={true}
                                setHardpoint={this.addWeapon}
                                weapons={unit.cortexes} />
                        } else {
                            return <HardpointAccordian
                                pointsRemaining={weaponPoints}
                                hardpoint={hardpoint}
                                index={index}
                                key={index}
                                label={weaponList[index]?.name}
                                isOpen={!weaponList[index]}
                                setHardpoint={this.addWeapon}
                                weapons={weapons.filter(w => w.location === hardpoint)} />
                        }
                    })}
                    {cantAfford && 
                    <span className='roboto pl2'>
                        {weaponList.map((weapon, index) => {
                        if(index < weaponList.length - 1) {
                            return weapon.name + ', '
                        } else {
                            return weapon.name
                        }
                    })}
                        <FontAwesomeIcon className='pointer grow pl2' size='lg' icon="edit" onClick={this.clearWeapons} />
                    </span>}
                </div>
            </div>
        );
    }
}