import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class MenuRow extends React.Component {
    render() {
        const { name, className, onClick } = this.props;
        return (
            <div className={'flex flex-row pointer dim items-center ' + className} key={name} onClick={onClick}>
                <FontAwesomeIcon size='1x' icon="plus-circle" />
                <div className='flex pl1'>{name}</div>                                
            </div>
        );
    }
}