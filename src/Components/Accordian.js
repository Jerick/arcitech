import React from 'react';
import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class Accordion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }
    onClick = () => {
        const { onToggle } = this.props;
        if (onToggle) {
            onToggle();
        } else {
            this.setState({ isOpen: !this.state.isOpen });
        }
    };
    render() {
        const { onToggle, className, childClassName, label, children, topAlignAnchor } = this.props;
        let isOpen = onToggle ? this.props.isOpen : this.state.isOpen;
        let childrenToRender = children && Array.isArray(children) ? [...children] : children;
        let hideHeader = !Array.isArray(children) ? isOpen : false;
        if (!label && Array.isArray(children)) childrenToRender.shift();
        let outerClass = 'flex-column ';
        if (hideHeader) outerClass = 'flex-row ';
        let pointerClass = '';
        if (topAlignAnchor) pointerClass = 'self-start mt1';
        return (
            <div className={'flex ' + outerClass + className}>
                <div className='flex flex-row flex-auto items-center pointer' onClick={this.onClick}>
                    {!isOpen &&
                        <FontAwesomeIcon icon='chevron-right' className={'f4 mr2 ' + pointerClass} style={{ width: '16px' }} />}
                    {isOpen &&
                        <FontAwesomeIcon icon='chevron-down' className={'f4 mr2 ' + pointerClass} style={{ width: '16px' }} />}
                    {!hideHeader && <div onClick={this.onClick} style={{ cursor: 'pointer' }}>
                        {label || children[0]}
                    </div>}
                </div>
                {isOpen &&
                    <div className={'flex w-100 ' + childClassName}>
                        {childrenToRender}
                    </div>}
            </div>
        );
    }
}

Accordion.propTypes = {
    label: PropTypes.node,
    isOpen: PropTypes.bool,
    childClassName: PropTypes.string,
    className: PropTypes.string,
    onToggle: PropTypes.func,
    topAlignAnchor: PropTypes.bool
};

Accordion.defaultProps = {
    childClassName: '',
    className: '',
    topAlignAnchor: false
};

export default Accordion;