import { Faction, Keywords } from './Enums';

export const Squads = [
    //Marcher Worlds
    { faction: [Faction.MW], key: 'q1', name: "Ranger Fire Team", keywords: [Keywords.Squad, Keywords.Ranger] },
    { faction: [Faction.MW], key: 'q2', name: "Ranger Heavy Support", keywords: [Keywords.Squad, Keywords.Ranger] },
    { faction: [Faction.MW], key: 'q3', name: "Ranger Infiltrators", keywords: [Keywords.Squad, Keywords.Ranger] },
    { faction: [Faction.MW], key: 'q4', name: "Dragoon Strike Team", keywords: [Keywords.Squad, Keywords.Dragoon] },
    { faction: [Faction.MW], key: 'q5', name: "Dragoon Assault Team", keywords: [Keywords.Squad, Keywords.Dragoon] },
    //ISA
    { faction: [Faction.ISA], key: 'q1', name: "Paladin Enforcers", keywords: [Keywords.Squad, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 'q2', name: "Paladin Annihilators", keywords: [Keywords.Squad, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 'q3', name: "Paladin Defenders", keywords: [Keywords.Squad, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 'q4', name: "Regulators", keywords: [Keywords.Squad, Keywords.Regulator] },
    { faction: [Faction.ISA], key: 'q5', name: "Tracers", keywords: [Keywords.Squad, Keywords.Tracers] },
    //AC
    { faction: [Faction.AC], key: 'q1', name: "Vassal Reavers", keywords: [Keywords.Squad, Keywords.Vassal] },
    { faction: [Faction.AC], key: 'q2', name: "Vassal Witch Hunters", keywords: [Keywords.Squad, Keywords.Vassal] },
    { faction: [Faction.AC], key: 'q3', name: "Vassal Raiders", keywords: [Keywords.Squad, Keywords.Vassal] },
    { faction: [Faction.AC], key: 'q4', name: "Talons", keywords: [Keywords.Squad, Keywords.Talon] },
    { faction: [Faction.AC], key: 'q5', name: "Synturions", keywords: [Keywords.Squad, Keywords.Synturions] },
    //EMP
    { faction: [Faction.EMP], key: 'q1', name: "Saber Strike Force", keywords: [Keywords.Squad, Keywords.Saber] },
    { faction: [Faction.EMP], key: 'q2', name: "Saber Guardians", keywords: [Keywords.Squad, Keywords.Saber] },
    { faction: [Faction.EMP], key: 'q3', name: "Saber Vanguards", keywords: [Keywords.Squad, Keywords.Saber] },
    { faction: [Faction.EMP], key: 'q4', name: "Antessors Eternal", keywords: [Keywords.Squad, Keywords.AntessorsEternal] },
    { faction: [Faction.EMP], key: 'q5', name: "Exalted Paragons", keywords: [Keywords.Squad, Keywords.Paragons] },
]