import { Faction, Keywords } from './Enums';

export const Solos = [
    //Marcher Worlds
    { faction: [Faction.MW], key: 's1', name: "Coalition Weaver", keywords: [Keywords.Solo] },
    { faction: [Faction.MW], key: 's2', name: "Combat Engineer", keywords: [Keywords.Solo] },    
    { faction: [Faction.MW], key: 's3', name: "Hunter", keywords: [Keywords.Solo] },
    { faction: [Faction.MW], key: 's4', name: "Dragoon Strike Team Leader", keywords: [Keywords.Solo, Keywords.Dragoon] },
    { faction: [Faction.MW], key: 's5', name: "Ranger Outrider", keywords: [Keywords.Solo, Keywords.Ranger] },
    //ISA
    { faction: [Faction.ISA], key: 's1', name: "Paladin Weaver", keywords: [Keywords.Solo, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 's2', name: "Paladin Commander", keywords: [Keywords.Solo, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 's3', name: "Paladin Siegebreaker", keywords: [Keywords.Solo, Keywords.Paladin] },
    { faction: [Faction.ISA], key: 's4', name: "Regulator Reeve", keywords: [Keywords.Solo, Keywords.Regulator] },
    { faction: [Faction.ISA], key: 's5', name: "Automech", keywords: [Keywords.Solo] },
    //AC
    { faction: [Faction.AC], key: 's1', name: "Marauder", keywords: [Keywords.Solo] },
    { faction: [Faction.AC], key: 's2', name: "Grafter", keywords: [Keywords.Solo] },
    { faction: [Faction.AC], key: 's3', name: "Immortal Weaver", keywords: [Keywords.Solo] },
    { faction: [Faction.AC], key: 's4', name: "Nekosphynx", keywords: [Keywords.Solo] },
    { faction: [Faction.AC], key: 's5', name: "Devine Tempest", keywords: [Keywords.Solo, Keywords.Champion] },
    { faction: [Faction.AC], key: 's6', name: "Vassal Boss", keywords: [Keywords.Solo, Keywords.Vassal] },
    //EMP
    { faction: [Faction.EMP], key: 's1', name: "Oculus", keywords: [Keywords.Solo] },
    { faction: [Faction.EMP], key: 's2', name: "Fulcrum", keywords: [Keywords.Solo] },
    { faction: [Faction.EMP], key: 's3', name: "Factotum ", keywords: [Keywords.Solo] },
    { faction: [Faction.EMP], key: 's4', name: "Eternal Metator", keywords: [Keywords.Solo, Keywords.Champion] },
    { faction: [Faction.EMP], key: 's5', name: "Paragon Commander", keywords: [Keywords.Solo, Keywords.Paragons] },
    { faction: [Faction.EMP], key: 's6', name: "Heavy Saber", keywords: [Keywords.Solo] },
    
]