import { Faction, Keywords } from './Enums';

export const Attachments = [
    //Marcher Worlds
    { faction: [Faction.MW], key: 'a1', name: "Warder", keywords: [Keywords.Attachment, Keywords.Ranger] },
    { faction: [Faction.MW], key: 'a2', name: "Dragoon Gunner", keywords: [Keywords.Attachment, Keywords.Dragoon] },
    //ISA
    { faction: [Faction.ISA], key: 'a1', name: "Paladin Aegis", keywords: [Keywords.Attachment, Keywords.Paladin]  },
    { faction: [Faction.ISA], key: 'a2', name: "Witch Hounds", keywords: [Keywords.Attachment, Keywords.Regulator]  },
    //AC
    { faction: [Faction.AC], key: 'a1', name: "Raker", keywords: [Keywords.Attachment, Keywords.Vassal] },
    { faction: [Faction.AC], key: 'a2', name: "Relikon", keywords: [Keywords.Attachment, Keywords.Talon] },
    //EMP
    { faction: [Faction.EMP], key: 'a1', name: "Aerolith", keywords: [Keywords.Attachment, Keywords.Saber] },
]