import { Hardpoint, Faction, Vehicle, Keywords } from './Enums';

export const Vehicles = [
    //Marcher Worlds
    {
        isVehicle: true,
        id: Vehicle.Razorbat,
        key: 'v1', 
        faction: [Faction.MW],
        name: "Razorbat",
        weaponPoints: 3,
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    {
        isVehicle: true,
        id: Vehicle.StormVulture,
        key: 'v2', 
        faction: [Faction.MW],
        name: "Storm Vulture",
        weaponPoints: 2,
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }],
        keywords: [Keywords.Vehicle, Keywords.Champion]
    },
    //Iron Star Alliance
    {
        isVehicle: true,
        id: Vehicle.Interceptor,
        key: 'v1', 
        faction: [Faction.ISA],
        name: "Interceptor",
        weaponPoints: 3,
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    //Aeternus Continuum
    {
        isVehicle: true,
        id: Vehicle.Scythe,
        key: 'v1', 
        faction: [Faction.AC],
        name: "Scythe",
        weaponPoints: 3,
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    //Empyrean
    {
        isVehicle: true,
        id: Vehicle.Zenith,
        key: 'v1', 
        faction: [Faction.EMP],
        name: "Zenith",
        weaponPoints: 3,
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
]