export enum Faction {
  MW = "m",
  ISA = "i",
  AC = "a",
  EMP = "e",
  WC = "w"
}

export enum Expansion {
  Base = "b",
  CollisionCourse = "c",
  ThousandWorlds = 't',
}

export enum Size {
  Intro = "i",
  Skirmish = "s",
  MidSized = "m",
  Full = "f"
}

export enum UnitTypes {
  Warjack = "Warjack",
  Squad = "Squad",
  Solo = "Solo",
  Hero = "Hero",
  Vehicle = "Vehicle"
}

export enum Hardpoint {
  Cortex = "Cortex",
  Head = "Head",
  Back = "Back",
  Chest = "Chest",
  Shoulder = "Shoulder",
  Arm = "Arm",
  Vehicle = "Vehicle",
}

export enum Warjack {
  DuskWolf = "Dusk Wolf",
  StrikeRaptor = "Strike Raptor",
  Firebrand = "Firebrand",
  Morningstar = "Morningstar",
  Headsman = "Headsman",
  Scourge = "Scourge",
  Nemesis = "Nemesis",
  Daemon = "Daemon",
  Sentinel = "Sentinel",
}

export enum Vehicle {
  Razorbat = "Razorbat",
  StormVulture = "Storm Vulture",
  FiddlerAndCo = "Fiddler and Co.",
  Interceptor = "Interceptor",
  Duchess = "Duchess",
  Scythe = "Scythe",
  Aenigma = "Aenigma",
  Zenith = "Zenith",
  ExecratorNix = "Execrator Nix"
}

export enum Keywords {
  Squad = 'Squad',
  Solo = 'Solo',
  Hero = 'Hero',
  Vehicle = 'Vehicle',
  Warjack = 'Warjack',
  Champion = 'Champion',
  Cortex = 'Cortex',
  Attachment = 'Attachment',
  Cadre = 'Cadre',

  //MW Specific
  Ranger = 'Ranger',
  Dragoon = 'Dragoon',
  //ISA Specific
  Paladin = 'Paladin',
  Regulator = 'Regulator',
  Tracers = 'Tracers',
  //AC Specific
  Vassal = 'Vassal',
  Talon = 'Talon',
  Synturions = 'Synturions',
  //Emp Specific
  Saber = 'Saber',
  AntessorsEternal = 'Antessors Eternal',
  Paragons = 'Paragons',
}
