import { Faction, Keywords } from './Enums';

export const Cadres = [
    //Marcher Worlds
    { 
        faction: [Faction.MW], 
        key: 'c1', 
        name: "Dragoon Air Cavalry",
        slotCost: 3,
        contains: ['q4', 'q5', 's4', 'v2'], 
        keywords: [Keywords.Cadre]
    },
    //ISA
    { 
        faction: [Faction.ISA], 
        key: 'c1', 
        name: "Alliance Regulators", 
        slotCost: 3,
        contains: ['q4', 'q5', 's4', 'w3'], 
        keywords: [Keywords.Cadre]
    },
    //AC
    { 
        faction: [Faction.AC], 
        key: 'c1', 
        name: "Terminus Cabal",
        slotCost: 3,
        contains: ['q4', 'q5', 's4', 's5'], 
        keywords: [Keywords.Cadre]
    },
    //EMP
    { 
        faction: [Faction.EMP], 
        key: 'c1', 
        name: "Harbingers of Cyriss",
        slotCost: 3,
        contains: ['q4', 'q5', 's4', 's5'], 
        keywords: [Keywords.Cadre]
     },
]