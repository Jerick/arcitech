import { Faction, Size, Hardpoint, Keywords } from "./Enums";
import { Squads } from "./Squads";
import { Solos } from "./Solos";
import { Warjacks } from "./Warjacks";
import { Heroes } from "./Heroes";
import { Vehicles } from "./Vehicles";
import { Weapons } from "./Weapons";
import { Cadres } from "./Cadres";

function getSolos(faction) {
    return [...Solos.filter(solo => {
        return solo.faction.includes(faction);
    })];
}

function getWarjacks(faction) {
    return [...Warjacks.filter(warjack => {
        return warjack.faction.includes(faction);
    })];
}

function getHeroes(faction) {
    return [...Heroes.filter(hero => {
        return hero.faction.includes(faction);
    })];
}

function getSquads(faction) {
    return [...Squads.filter(squad => {
        return squad.faction.includes(faction);
    })];
}

function getVehicles(faction) {
    return [...Vehicles.filter(vehicle => {
        return vehicle.faction.includes(faction);
    })];
}

function getWeapons(model) {
    return [...Weapons.filter(weapon => {
        return weapon.availabilty.includes(model.id);
    })];
}

function getCadres(faction) {
    return [...Cadres.filter(cadre => {
        return cadre.faction.includes(faction);
    })];
}

function getWeaponPoints(model) {
    let modelFound = Heroes.filter(hero => { return (hero.name === model.name) })[0];
    if(!modelFound) modelFound = Vehicles.filter(vehicle => { return (vehicle.name === model.name) })[0];
    if(!modelFound) modelFound = Warjacks.filter(warjack => { return (warjack.name === model.name) })[0];
    if(modelFound.weaponPoints) {
        return modelFound.weaponPoints;
    } else return -1;
}

function generateSideBar(faction) {
    return {
        Solos: getSolos(faction).filter(solo => {return !solo.keywords?.includes(Keywords.Champion)}),
        Warjacks: getWarjacks(faction).filter(warjack => {return !warjack.keywords?.includes(Keywords.Champion)}),
        Squads: getSquads(faction).filter(squad => {return !squad.keywords?.includes(Keywords.Champion)}),
        Heroes: getHeroes(faction).filter(hero => {return !hero.keywords?.includes(Keywords.Champion)}),
        Vehicles: getVehicles(faction).filter(vehicle => {return !vehicle.keywords?.includes(Keywords.Champion)}),
        Cadres: getCadres(faction),
    }
}

function generateLoadoutSideBar(model) {
    let weapons = getWeapons(model);
    return {
        Shoulder: weapons.filter(w => w.location === Hardpoint.Shoulder),
        Arm: weapons.filter(w => w.location === Hardpoint.Arm),
        Back: weapons.filter(w => w.location === Hardpoint.Back),
        Head: weapons.filter(w => w.location === Hardpoint.Head),
        Vehicle: weapons.filter(w => w.location === Hardpoint.Vehicle),
        Cortex: [...model.cortexes]
    }
}

function getFaction(value) {
    switch (value) {
        case Faction.MW:
            return { faction: Faction.MW, factionName: "Marcher Worlds", logo: 'mw.png' };
        case Faction.ISA:
            return { faction: Faction.ISA, factionName: "Iron Star Alliance", logo: 'isa.png' };
        case Faction.AC:
            return { faction: Faction.AC, factionName: "Aeternus Continuum", logo: 'ac.png' };
        case Faction.EMP:
            return { faction: Faction.EMP, factionName: "Empyrean", logo: 'emp.png' };
        case Faction.WC:
            return { faction: Faction.WC, factionName: "Wild Card" };
        default:
            return null;
    }
}

function getSize(value) {
    switch (value) {
        case Size.Intro:
            return { unitLimit: 6, heroLimit: 0, size: 'i' };
        case Size.Skirmish:
            return { unitLimit: 8, heroLimit: 1, size: 's' };
        case Size.MidSized:
            return { unitLimit: 11, heroLimit: 2, size: 'm' };
        case Size.Full:
            return { unitLimit: 15, heroLimit: 3, size: 'f' };
        default:
            return null;
    }
}

function createUrl(data) {    
    let string = '';
    string += data.size;
    string += data.faction;
    for (let i = 0; i < data.list.length; i++) {
        if (data.list[i].isWarjack || data.list[i].isVehicle) {
            string += data.list[i].key;
            let weaponString = '';
            if (data.list[i].weaponList) {
                data.list[i].weaponList.forEach(weapon => {
                    if (weapon !== undefined) {
                        weaponString += weapon.key;
                    }
                });
            }
            string += weaponString + 'x';
        } else {
            string += data.list[i].key;
        }
    }

    return string;
}

function getLoadout(unit, string) {
    let unitWeapons = getWeapons(unit);
    let loadout = [];
    let substringStart = 0;
    if (string.length % 2 === 1) {
        loadout.push(unit.cortexes.find(cortex => cortex.key === string[0]));
        substringStart = 1;
    }
    if (string.substring(substringStart).length > 0) {
        if (substringStart === 0 && unit.isWarjack) {
            loadout.push(undefined);
        }
        string.substring(substringStart).match(/.{2}/g).forEach(key => {
            loadout.push(unitWeapons.find(weapon => weapon.key === key));
        });
    }
    return loadout;
}

function updateTextList(list, url, factionName) {    
    let flatList = [];
    list.forEach((unit, index) => {
        let i = flatList.findIndex(u => { return (!unit.hardpoints && u.name === unit.name) });
        if(i < 0) {                
            flatList.push({...unit, unitCount: list.filter(u => (u.name === unit.name)).length, index: index})
        }
    })

    let textList = ''        
    textList += 'https://www.arcitech.net/' + url + '\n\n';
    textList += factionName + '\n\n'
    flatList.forEach((unit) => {
        if(unit.hardpoints) {
            let weaponString = '';
            unit.weaponList.forEach((weapon, index) => {
                if(index < unit.weaponList.length - 1) {
                    weaponString += weapon.name + ', ';
                } else {
                    weaponString += weapon.name;
                }
            });
            textList += unit.name + '\n ꞏ ' + weaponString + '\n';
        } else {
            if(unit.unitCount > 0) {
                textList += unit.unitCount + 'x ' + unit.name + '\n' 
            } else {
                textList += unit.name + '\n';
            }
        }
    })
    return textList;
}

function parseUrl(string) {
    let data = { ...getSize(string[0]), ...getFaction(string[1]) };
    let list = [];
    try {
        let tokenized = string.match(/([a-z][0-9]+[x]*)/g);
        if (tokenized) {
            tokenized.forEach(token => {
                let unit = null;
                let split = null;
                switch (token[0]) {
                    case 'c': // Cadres
                        list.push(getCadres(data.faction).find(model => model.key === token));
                        break;
                    case 's': // Solos
                        list.push(getSolos(data.faction).find(model => model.key === token));
                        break;
                    case 'q': // Squads
                        list.push(getSquads(data.faction).find(model => model.key === token));
                        break;
                    case 'w': // Warjacks
                    case 'v': // Vehicles                        
                        split = token.match(/([wv]\d)(\d*)(x)/);
                        let unitKey = split[1];
                        if (unitKey[0] === 'w') {
                            unit = { ...getWarjacks(data.faction).find(model => model.key === unitKey) }
                        } else {
                            unit = { ...getVehicles(data.faction).find(model => model.key === unitKey) }
                        }
                        if (split[1]) {
                            unit.weaponList = getLoadout(unit, split[2]);
                            unit.weaponList.forEach(weapon => {
                                if (weapon.cost) {
                                    unit.weaponPoints -= weapon.cost;
                                }
                            })
                        }
                        list.push(unit);
                        break;
                    case 'h': // Heroes                             
                        split = token.match(/(h\d)(\d*)(x)/);
                        if (split) {
                            let unitKey = null;
                            unitKey = split[1];
                            unit = { ...getHeroes(data.faction).find(model => model.key === unitKey) }
                            if (unit.isVehicle) {
                                if (split[1]) {
                                    unit.weaponList = getLoadout(unit, split[2]);
                                    unit.weaponList.forEach(weapon => {
                                        if (weapon.cost) {
                                            unit.weaponPoints -= weapon.cost;
                                        }
                                    })
                                }
                            }
                            list.push(unit);
                        } else {
                            list.push(getHeroes(data.faction).find(model => model.key === token));
                        }
                        break;
                    case 'i': // Wildcards
                        list.push(getHeroes(data.faction).find(model => model.key === token));
                        break;
                    default:
                        break;
                }
            })
        }
    } catch { }
    finally {
        data.list = list;
        data.textList = updateTextList(list, string, data.factionName);
        return data;
    }
}

function findModel(code, faction) {
    switch (code[0]) {
        case 's': // Solos
            return getSolos(faction).find(model => model.key === code);
        case 'q': // Squads
            return getSquads(faction).find(model => model.key === code);
        case 'w': // Warjacks
            return getWarjacks(faction).find(model => model.key === code);
        case 'v': // Vehicles                        
            return getVehicles(faction).find(model => model.key === code);
        case 'h': // Heroes                             
            return getHeroes(faction).find(model => model.key === code);
        case 'i': // Wildcards
            return getHeroes(faction).find(model => model.key === code);            
        default:
            break;
    }
}

export { getSolos, getWarjacks, getSquads, getHeroes, getWeapons, generateSideBar, generateLoadoutSideBar, createUrl, parseUrl, getFaction, getSize, getWeaponPoints, updateTextList, findModel };