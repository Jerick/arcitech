import { Vehicle, Faction, Hardpoint } from './Enums';

export const Heroes = [
    //Marcher Worlds
    { isHero: true, limit: 1, faction: [Faction.MW], key: 'h1', name: "Artemis Fang" },
    {
        isVehicle: true, 
        isHero: true, 
        limit: 1, 
        id: Vehicle.FiddlerAndCo, 
        key: 'h2', 
        faction: [Faction.MW], 
        name: "Fiddler & Co.", 
        weaponList: [], 
        weaponPoints: 3, 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    { isHero: true, limit: 1, faction: [Faction.MW], key: 'h3', name: "Warchief Malek Sezzar" },
    //ISA
    { isHero: true, limit: 1, faction: [Faction.ISA], key: 'h1', name: "Justicar Voss" },
    {
        isVehicle: true, 
        isHero: true, limit: 1, 
        id: Vehicle.Duchess, 
        key: 'h2', 
        faction: [Faction.ISA], 
        name: "Duchess", 
        weaponPoints: 3, 
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    { isHero: true, limit: 1, faction: [Faction.ISA], key: 'h3', name: "Major Aysa Drayce" },
    //AC
    { isHero: true, limit: 1, faction: [Faction.AC], key: 'h1', name: "Hiertheos Raxis" },
    {
        isVehicle: true, 
        isHero: true, 
        limit: 1, 
        id: Vehicle.Aenigma, 
        key: 'h2', 
        faction: [Faction.AC], 
        name: "Aenigma", 
        weaponPoints: 3, 
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    { isHero: true, limit: 1, faction: [Faction.AC], key: 'h3', name: "Phaetheon, the Whisper of Death" },
    //Emp
    { isHero: true, limit: 1, faction: [Faction.EMP], key: 'h1', name: "Astreus, Aeon of the First Magnitude" },
    {
        isVehicle: true, 
        isHero: true, 
        limit: 1, 
        id: Vehicle.ExecratorNix, 
        key: 'h2', 
        faction: [Faction.EMP], 
        name: "Execrator Nix", 
        weaponPoints: 3, 
        weaponList: [], 
        hardpoints: [{ id: Hardpoint.Vehicle, count: 1 }]
    },
    { isHero: true, limit: 1, faction: [Faction.EMP], key: 'h3', name: "Aurelion, Aeon Weaver" },
    //Wild Cards
    { isHero: true, limit: 1, key: 'i1', faction: [Faction.ISA, Faction.AC, Faction.WC], name: "Baron Cassius Mooregrave" },
    { isHero: true, limit: 1, key: 'i2', faction: [Faction.ISA, Faction.MW, Faction.WC], name: "Voitek Sudel, Bounty Hunter" },
    { isHero: true, limit: 1, key: 'i3', faction: [Faction.AC, Faction.MW, Faction.WC], name: "Captain Jax Redblade" },
    { isHero: true, limit: 1, key: 'i4', faction: [Faction.EMP, Faction.AC, Faction.WC], name: "Harlan Sek, the Curator" },
    { isHero: true, limit: 1, key: 'i5', faction: [Faction.EMP, Faction.ISA, Faction.WC], name: "Doctor Myra Hurst" },
    { isHero: true, limit: 1, key: 'i6', faction: [Faction.EMP, Faction.MW, Faction.WC], name: "Corebus" },    
    { isHero: true, limit: 1, key: 'i7', faction: [Faction.ISA, Faction.EMP, Faction.MW, Faction.WC], name: "Fenrik Leech, Free Agent" },
    { isHero: true, limit: 1, key: 'i8', faction: [Faction.ISA, Faction.EMP, Faction.MW, Faction.WC], name: "Axel for Hire" },
    { isHero: true, limit: 1, key: 'i9', faction: [Faction.ISA, Faction.EMP, Faction.MW, Faction.WC], name: "Master Tulcan" },
]