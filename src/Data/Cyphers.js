import { Faction, Expansion } from './Enums';

export const Cyphers = [
    // Harmonic
    { 
        key: 'a',         
        name: "Arcane Synthesis", 
        text: "Target a friendly unit. \n\n You can immediately charge the unit with any amount of Arc up to its limit.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'b',         
        name: "Encrypted Command", 
        text: "Target a friendly solo. \n\n Remove the activation token from the solo.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'c',         
        name: "Recall Initiative", 
        text: "Target a friendly unit. \n\n Recall the unit.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'd',         
        name: "Aggression Theorem", 
        text: "Target a friendly unit model. \n\n The target model can immediately make one melee or anged attack.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'e',         
        name: "Divination Algorithm", 
        text: "Target a friendly unit. \n\n This unit's ranged weapons gain +1 RNG and POW. \n\n Divination Algorithm expires at the end of the Pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'f',         
        name: "Revelation Matrix", 
        text: "Target a friendly unit. \n\n The unit can ignore the Stealth special rule. \n\n Revelation Matrix expires at the end of the Pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'g',         
        name: "Dominion Imperative", 
        text: "Each friendly solo can immediately move up to 2\".",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'h',         
        name: "Photochimeric Mantle", 
        text: "Target a friendly unit. \n\n The unit gains Stealth. a model with Stealth cannot be targeted by attacks made by models more then 8\" away. \n\n Photochimeric Mantle expires at the end of the pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'i',         
        name: "Psychodynamic Infuser", 
        text: "Target a friendly light vehicle. \n\n Remove the activation token from the light vehicle, then place this card in the discard pile.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'j',         
        name: "Canker Bane", 
        text: "Target a friendly unit. \n\n Models hit by melee attacks made by the affected unit suffer the Corrosion continuous effect. \n\n Canker Bane expires at the end of the Pulse round.",
        faction: [Faction.AC], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'k',         
        name: "Cyclonic Vitalizer", 
        text: "Target a friendly unit. \n\n The unit can move up to 3\" at the start of each of your turns. Cyclonic Vitalizer expires at the end of the Pulse round.",
        faction: [Faction.MW], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'l',         
        name: "Reclamation Principle", 
        text: "Target a friendly unit. \n\n Affected models gain +2 MAT. When an affected model destroys an enemy model with a melee attack, you can remote 1 damage point from a friendly warjack within 5\" of the affected model. \n\n Reclamation Principle expires at the ened of the Pulse round.",
        faction: [Faction.MW], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'm',         
        name: "Eschatonic Well", 
        text: "Target a friendly warrior model. \n\n Place a void gate anywhere within 5\" of the target model. You can charge this void gate with up to 3 Arc. \n\n After this card is played, remote it from the game. (Do not discard it.)",
        faction: [Faction.ISA], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'n',         
        name: "Excogitation Conspectus", 
        text: "Target a friendly unit. \n\n When a model is targeted by an enemy Cypher while within 5\" of a model affected by this Cypher, you can immediately choose one Cypher card from your discard pile and return it to your hand. \n\n Excogiation Cospectus expires at the end of the Pulse round.",
        faction: [Faction.EMP], 
        expansion: Expansion.CollisionCourse
    },
    { 
        key: 'o',         
        name: "Arcanesscent Regenerator", 
        text: "Target a friendly void gate. \n\n You can immediately charge the void gate with any amount of Arc up to its limit.",
        faction: [Faction.EMP], 
        expansion: Expansion.CollisionCourse
    },

    // Fury
    {
         key: 'a', 
         name: "Cryo Lock", 
         pow: '-', 
         text: "Target an enemy unit model in range. \n\n Make a Fury attack roll against the target model. If this attack hits, it does no damage. Instead, the hit unit gains an activation token. \n\n Models that are immune to cold damage are not affected by this Fury." ,
         faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP],
         expansion: Expansion.Base
    },
    { 
        key: 'b', 
        name: "Null Collider", 
        pow: '3', 
        text: "Target an enemy model in range. \n\n Make an attack roll against the target model. If the attack hits, in addition to a damage roll, clear 1 Arc from the unit or void gate hit. \n\n This Fury causes energy Damage",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'c',         
        name: "Velocity Projector", 
        pow: '4', 
        text: "Target an enemy model in range. \n\n Make an attack roll against the target model. If the attack hits, the model is slammed 3\" directly away from the model the Fury is being channeled through before suffering a damage roll. Models with an equal or smaller base that are contacted by the slammed model suffer a collateral damage roll equal to the POW of this card. \n\n This Fury causes force damage.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'd', 
        name: "Malediction Rubric", 
        pow: '4', 
        text: "Target an enemy unit model in range. \n\n Make an attack roll against the target model. If the attack hits, enemy Cypher cards on the unit hit expire and the model hit suffers a damage roll. \n\n This Fury causes energy damage.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'e', 
        name: "Instability Equation", 
        pow: '4', 
        text: "Target an enemy unit model in range. \n\n When this attack hits its target, resolve the attack against the target as normal. Additionally, when this attack hits its target, the two models closest to the target that are also within 2\" suffer blast damage rolls equal to the POW of this attack. If this attack misses its target, the target still suffers a blast damage roll equal to the POW of this attack. \n\n This Fury causes blast damage.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'f', 
        name: "Pyrokinetic Surge", 
        pow: '3', 
        text: "Target an enemy model in range. \n\n Make an attack roll against the target model. If the attack hits, the model hit suffers the Fire continuous effect in addition to suffering a damage roll. \n\n This Fury causes fire damage.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },

    //Geometric
    { 
        key: 'a',         
        name: "Reiteration Complex", 
        text: "Target a friendly squad. \n\n During the squad's activation, each model in the squad can make one additional ranged attack. \n\n Reiteration Complex expires at the end of this turn.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'b',         
        name: "Temporal Cycle", 
        text: "Target a friendly squad. \n\n Remove the activation token from this squad.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'c',         
        name: "Displacement Index", 
        text: "Target a friendly squad. \n\n The squad can immediately move up to 3\".",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'd',         
        name: "Force Barrier", 
        text: "Target a friendly squad. \n\n Affected models gain cover. \n\n Force Barrier expires at the end of the Pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'e',         
        name: "Plexus Densifier", 
        text: "Target a friendly squad. \n\n The squad gains +2 ARM but suffers -1 SPD. \n\n Plexus Densifier expires at the end of the Pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'f',         
        name: "Mortality Destabilizer", 
        text: "Target a friendly squad. \n\n Return one destroyed non-attachment model to this squad. Place the returned model within 2\" of another model in the squad.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },

    //Overdrive
    { 
        key: 'a',         
        name: "Interdiction Protocol", 
        text: "Target a friendly warjack. \n\n Friendly warrior models withing 5\" of this warjack gain +2 DEF. \n\n Interdiction Protocol expires at the end of the Pulse round.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'b',         
        name: "Momentum Calibrator", 
        text: "Target a friendly warjack. \n\n When the warjack hits a model with an equal or smaller base with a melee attack. before dmage is rolled, the model hit is slammed directly away from the warjack. Roll a number of action dice equal to the STR of the warjack. For every strike rolled, move the model hit 1\". Collateral damage is equal to the STR of the attacking warjack. \n\n This card expires at the end of this turn.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'c',         
        name: "Annihilation Vector", 
        text: "Target a friendly warjack. \n\n The warjack can immediately make one melee attack against an enemy model within 1\". If the attack hits, the model hit suffers a damage roll with a POW equal to the attacking warjack's STR. This attack causes kinetic damage. Additionally, if the model hit was a warjack, it suffers the System Failure continuous effect.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'd',         
        name: "Impulse Inducer", 
        text: "Target a friendly warjack. \n\n Remove the activation token from the warjack.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'e',         
        name: "Ascension Catalyst", 
        text: "Target a friendly warjack. \n\n The warjack gains +2 SPD and the Flight special rule. \n\n Ascension Catalyst expires at the end of this turn.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
    { 
        key: 'f',         
        name: "Kinetic Accelerator", 
        text: "Target a friendly warjack. \n\n When this warjack makes a melee attack during its activation, make a melee attack roll against each enemy model within the weapon's RNG. Models hit suffer the full effects and damage of the attack they were hit with. \n\n Kinetic Accelerator expires at the end of this turn.",
        faction: [Faction.ISA, Faction.AC, Faction.MW, Faction.EMP], 
        expansion: Expansion.Base
    },
]