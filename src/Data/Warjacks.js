import { Hardpoint, Warjack, Faction, Keywords } from './Enums';

export const Warjacks = [
    //Marcher Worlds
    {
        isWarjack: true,
        id: Warjack.DuskWolf,
        key: 'w1',
        faction: [Faction.MW],
        name: "Dusk Wolf",
        weaponPoints: 5,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 1 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Fighter" },
            { key: '2', isCortex: true, name: "Neural Net" },
            { key: '3', isCortex: true, name: "Scout" },
            { key: '4', isCortex: true, name: "Sniper" },
        ]
    },
    {
        isWarjack: true,
        id: Warjack.StrikeRaptor,
        key: 'w2',
        faction: [Faction.MW],
        name: "Strike Raptor",
        weaponPoints: 8,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 2 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Ace" },
            { key: '2', isCortex: true, name: "Bomber" },
            { key: '3', isCortex: true, name: "Dog Fighter" },
            { key: '4', isCortex: true, name: "Precision" },
        ]
    },
    //ISA
    {
        isWarjack: true,
        id: Warjack.Firebrand,
        key: 'w1',
        faction: [Faction.ISA],
        name: "Firebrand",
        weaponPoints: 5,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 1 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Jack Hunter" },
            { key: '2', isCortex: true, name: "Neural Web" },
            { key: '3', isCortex: true, name: "Recon Cortex" },
            { key: '4', isCortex: true, name: "Reflex Cortex" },
        ]
    },
    {
        isWarjack: true,
        id: Warjack.Morningstar,
        key: 'w2',
        faction: [Faction.ISA],
        name: "Morningstar",
        weaponPoints: 8,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 2 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Deadeye Cortex" },
            { key: '2', isCortex: true, name: "Defender" },
            { key: '3', isCortex: true, name: "Dauntless" },
            { key: '4', isCortex: true, name: "Exchanger" },
        ]
    },
    {
        isWarjack: true,
        id: Warjack.Headsman,
        key: 'w3',
        faction: [Faction.ISA],
        name: "Headsman",
        weaponPoints: 8,
        weaponList: [ { name: "Eye of Sorrows"}, { name: "Tractor Cannon"}, { name: "Heavy Fusion Blade"} ],
        hardpoints: [],
        cortexes: [],
        keywords: [Keywords.Warjack, Keywords.Champion]
    },
    //AC
    {
        isWarjack: true,
        id: Warjack.Scourge,
        key: 'w1',
        faction: [Faction.AC],
        name: "Scourge",
        weaponPoints: 5,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 2 },
            { id: Hardpoint.Arm, count: 1 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Killer" },
            { key: '2', isCortex: true, name: "Misanthrope" },
            { key: '3', isCortex: true, name: "Revenger" },
            { key: '4', isCortex: true, name: "Spider" },
        ]
    },
    {
        isWarjack: true,
        id: Warjack.Nemesis,
        key: 'w2',
        faction: [Faction.AC],
        name: "Nemesis",
        weaponPoints: 8,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Shoulder, count: 2 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Battle Cruiser" },
            { key: '2', isCortex: true, name: "Impulse Reciprocator" },
            { key: '3', isCortex: true, name: "Oracle" },
            { key: '4', isCortex: true, name: "Ghost" },
        ]
    },
    //EMP
    {
        isWarjack: true,
        id: Warjack.Daemon,
        key: 'w1',
        faction: [Faction.EMP],
        name: "Daemon",
        weaponPoints: 5,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Back, count: 1 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Eidolon" },
            { key: '2', isCortex: true, name: "Guardian" },
            { key: '3', isCortex: true, name: "Hound" },
            { key: '4', isCortex: true, name: "Wraith" },
        ]
    },
    {
        isWarjack: true,
        id: Warjack.Sentinel,
        key: 'w2',
        faction: [Faction.EMP],
        name: "Sentinel",
        weaponPoints: 8,
        weaponList: [],
        hardpoints: [
            { id: Hardpoint.Cortex, count: 1 },
            { id: Hardpoint.Back, count: 1 },
            { id: Hardpoint.Chest, count: 1 },
            { id: Hardpoint.Arm, count: 2 },
        ],
        cortexes: [
            { key: '1', isCortex: true, name: "Avatar" },
            { key: '2', isCortex: true, name: "Divinator" },
            { key: '3', isCortex: true, name: "Hellion" },
            { key: '4', isCortex: true, name: "Numen" },
        ]
    },
]