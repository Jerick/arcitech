import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

library.add(fas);
library.add(far);

ReactDOM.render(
  <React.StrictMode>   
    <App path='/'/>
  </React.StrictMode>,
  document.getElementById('root')
);